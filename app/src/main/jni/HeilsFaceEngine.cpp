#include "HeilsFaceEngine.h"



HeilsFaceEngine::~HeilsFaceEngine() {
    m_faceEngine->Destroy();
}

HeilsFaceEngine::HeilsFaceEngine() {
    m_faceEngine = FaceEngine::GetInstance();
}

void HeilsFaceEngine::SetFaceDetectModel(const char *_modelDir) {
    m_faceEngine->SetFaceDetectModel(_modelDir);
}

void HeilsFaceEngine::SetFaceFeatureModel(const char *_modelDir) {
    m_faceEngine->SetFaceFeatureModel(_modelDir);
}

void HeilsFaceEngine::SetFaceMarksModel(const char *_modelDir) {
    m_faceEngine->SetFaceMarksModel(_modelDir);
}

void HeilsFaceEngine::SetMinFaceSize(int _minSize){
    m_faceEngine->SetMinFaceSize(_minSize);
}

int HeilsFaceEngine::FaceFeature(const char* _inputImgPath, float* _faceFea){
    m_faceEngine->FaceFeature(_inputImgPath,_faceFea);
}
int HeilsFaceEngine::FaceCropImage(const char* _inputImgPath, const char* _outImgPath){
    m_faceEngine->FaceCropImage(_inputImgPath, _outImgPath);
}
float HeilsFaceEngine::CalSimilar(const char* _inputImgPath1, const char* _inputImgPath2){
    m_faceEngine->CalSimilar(_inputImgPath1, _inputImgPath2);
}


extern "C" {

void init_engine(HeilsFaceEngine** _heilsEngine) {
    *_heilsEngine = new HeilsFaceEngine();
}

void destroy_engine(HeilsFaceEngine** _heilsEngine) {
    delete (*_heilsEngine);
    _heilsEngine = NULL;
}
}
