#pragma once
#include <opencv2/opencv.hpp>
#include "Struct.h"
#include <vector>
#define pi 3.14159265

class Posture
{
public:
	Posture(const Bbox&, const std::vector<float>&);
	~Posture() {};
	int getPos();
	void cropByPosture(const cv::Mat&, cv::Mat&);
private:
	std::vector<cv::Point> cPoint9;
	std::vector<cv::Point> cPoint5;
};
