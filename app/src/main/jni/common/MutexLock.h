#pragma once

#ifndef __MUTEX_H__  
#define __MUTEX_h__

#ifdef __ANDROID__
#include <pthread.h>
class MutexLock
{
private:
	pthread_mutex_t m_mutex;
public:
	MutexLock();
	virtual ~MutexLock();
private:
    MutexLock(const MutexLock&) {};
    MutexLock& operator=(const MutexLock&) {};
public:
	int lock();
	int unlock();
	int trylock();
};
#endif
#endif