#include "Struct.h"
#include <cmath>

bool cmpScore(Bbox lsh, Bbox rsh) {
	if (lsh.score < rsh.score)
		return true;
	else
		return false;
}

bool cmpArea(Bbox lsh, Bbox rsh) {
	return lsh.area > rsh.area;
}

float distance(cv::Point p1, cv::Point p2) {
	int dx = p1.x - p2.x;
	int dy = p1.y - p2.y;
	return sqrt(dx * dx + dy * dy);
}
