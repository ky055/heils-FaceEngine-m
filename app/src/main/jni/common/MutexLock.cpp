#include "MutexLock.h"

#ifdef __ANDROID__
MutexLock::MutexLock()
{
	pthread_mutex_init(&m_mutex, NULL);
}

MutexLock::~MutexLock()
{
	pthread_mutex_destroy(&m_mutex);
}

int MutexLock::lock()
{
	return  pthread_mutex_lock(&m_mutex);
}

int MutexLock::unlock()
{
	return pthread_mutex_unlock(&m_mutex);
}

int MutexLock::trylock()
{
	return pthread_mutex_trylock(&m_mutex);
}
#endif