#include "Mark.h"

Mark::Mark(std::string path) {
	squeezenet.load_param((path + "mark.params").c_str());
	squeezenet.load_model((path + "mark.bin").c_str());
}

Mark::~Mark() {
	squeezenet.clear();
}

void Mark::getMarkPoint(const cv::Mat& subimage, std::vector<float>& markPoint)
{
	cv::Mat sub3;
	cvtColor(subimage, sub3, CV_RGB2GRAY);

	cv::Mat sub2;
	subimage.convertTo(sub2, CV_32FC1);

	cv::Mat tmp_m, tmp_sd;
	float m = 0, sd = 0;
	cv::meanStdDev(sub2, tmp_m, tmp_sd);
	m = tmp_m.at<double>(0, 0);
	sd = tmp_sd.at<double>(0, 0);

	ncnn::Mat subin = ncnn::Mat::from_pixels_resize(sub3.data, ncnn::Mat::PIXEL_GRAY, sub3.cols, sub3.rows, 60, 60);
	mean_vals[0] = m;

	subin.substract_mean_normalize(mean_vals, 0);
	for (int i = 0; i < subin.h; i++) {
		float* p = subin.row(i);
		for (int j = 0; j < subin.w; j++) {
			*p = (*p) / (0.000001 + sd);
			p++;
		}
	}

	ncnn::Extractor ex = squeezenet.create_extractor();
	ex.set_light_mode(true);

	ex.input("data", subin);
	ncnn::Mat out;
	int a = ex.extract("Dense3", out);
	//std::vector<float> feat;

	for (int i = 0; i < out.w; i++)
	{
		const float* prob = static_cast<float*>(out.data) + i;
		markPoint.push_back(prob[0]);
	}
}
