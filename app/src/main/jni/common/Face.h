#pragma once
#include "Mark.h"
#include "Rotate.h"
#include "Detect.h"
#include "Posture.h"
#include "Recognize.h"
#include "EngineUtils.h"

class Face
{
public:
	Face() {};
	Face(std::string, std::string);
	~Face();
	void setPath(std::string, std::string);
	int startDetect(int);
	cv::Mat getDetectResult();
	void startRotate();
	cv::Mat getRotateResult();
	void startMark();
	cv::Mat getMarkResult();
	int getPosture();
	void startCrop();
	cv::Mat getCropResult();
	void startFeature();
	float* getFeatureArray();
	float calSimilarity(float*);
	cv::Mat getRawImage();
private:
	Detect* mDetect;
	Rotate* mRotate;
	Mark* mMark;
	Posture* mPosture;
	Recognize* mRecognize;
	cv::Mat rawImage, detRes, rotRes, markRes, cropRes;
	Bbox maxBbox;
	std::vector<Bbox> finalBbox;
	bool hasDetected, hasMarked, hasRotated, hasFeatured, hasCropped;
	std::vector<float> markPoint;
	float* featureArray;
	std::string modelPath;
};

/*static Detect* mDetect;
static Rotate* mRotate;
static Mark* mMark;
static Posture* mPosture;
static Recognize* mRecognize;
static cv::Mat rawImage, detRes, rotRes, markRes, cropRes;
static Bbox maxBbox;
static std::vector<Bbox> finalBbox;
static bool hasDetected, hasMarked, hasRotated, hasFeatured, hasCropped;
static std::vector<float> markPoint;
static float* featureArray;
static std::string Path;*/