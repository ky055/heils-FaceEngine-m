#include "Recognize.h"
Recognize::Recognize(std::string path) {
	Recognet.load_param((path + "feature.param").c_str());
	Recognet.load_model((path + "feature.bin").c_str());
}

Recognize::~Recognize() {
	Recognet.clear();
}

void Recognize::SetThreadNum(int threadNum) {
	threadnum = threadNum;
}

void Recognize::start(const ncnn::Mat&ncnn_img, float* features) {
	ncnn::Extractor ex = Recognet.create_extractor();
	ex.set_num_threads(threadnum);
	ex.set_light_mode(true);
	ex.input("data", ncnn_img);
	ncnn::Mat out;
	ex.extract("pre_fc1", out);
	ncnn::Mat test;
	for (int j = 0; j < 512; j++) {
		features[j] = out[j];
	}
}
