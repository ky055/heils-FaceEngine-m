#pragma once
#include "Struct.h"
#include <string>
#include <vector>
#include "net.h"

class Detect {
public:
	Detect(std::string);
	virtual ~Detect();
	void SetThreadNum(int);
	void getFinalBox(const ncnn::Mat&, std::vector<Bbox>&);

private:
	void generateBbox(ncnn::Mat&,const ncnn::Mat&, std::vector<Bbox>&, float);
	void nms(std::vector<Bbox>&, float, std::string modelname = "Union");
	void refine(std::vector<Bbox>&, int, int, bool);
	void PNet();
	void RNet();
	void ONet();
	ncnn::Net Pnet, Rnet, Onet;
	ncnn::Mat img;
	const float nms_threshold[3] = { 0.5f, 0.7f, 0.7f };
	const float mean_vals[3] = { 127.5, 127.5, 127.5 };
	const float norm_vals[3] = { 0.0078125, 0.0078125, 0.0078125 };
	const int MIN_DET_SIZE = 12;
	std::vector<Bbox> firstBbox_, secondBbox_, thirdBbox_;
	int img_w, img_h;
	const float threshold[3] = { 0.8f, 0.8f, 0.6f };
	int minsize = 40;
	const float pre_facetor = 0.709f;
	int threadnum = 1;
};