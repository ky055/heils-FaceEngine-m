#pragma once

#ifndef __SINGLE_FaceEngine_H__  
#define __SINGLE_FaceEngine_H__ 
#include <iostream>
#include <vector>
#ifdef __ANDROID__
#include "MutexLock.h"
#endif // _Android
#include "Struct.h"
#include "Recognize.h"
#include "Detect.h"
#include "Mark.h"



class FaceEngine
{
public:
	static FaceEngine *GetInstance();
	static void Destroy();
	void SetFaceDetectModel(const char* _modelDir);
	void SetFaceFeatureModel(const char* _modelDir);
	void SetFaceMarksModel(const char* _modelDir);
	void SetMinFaceSize(int _minSize);

	int FaceFeature(const char* _inputImgPath, float* _faceFea);
	int FaceCropImage(const char* _inputImgPath, const char* _outImgPath);
	float CalSimilar(const char* _inputImgPath1, const char* _inputImgPath2);
	

private:
	FaceEngine();
	virtual ~FaceEngine();
	FaceEngine(const FaceEngine&) {};
	FaceEngine& operator=(const FaceEngine&) {};
	cv::Mat FaceRotate(cv::Mat& _faceImg, Bbox _faceBbox);
	void DetectFace(cv::Mat& _rawImage, int _minSize, std::vector<Bbox>& _faceBboxes);
	
	Recognize* m_Recognizor;
	Detect*	 m_Detector;
	Mark* m_LandMarker;
	int m_minSize;

private:
	static FaceEngine *m_pInstance;
#ifdef __ANDROID__
	static MutexLock m_Mutex;
#endif

};

#endif