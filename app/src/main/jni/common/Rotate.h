#pragma once
#include "opencv2/opencv.hpp"
#include "Struct.h"

class Rotate
{
public:
	Rotate() {};
	~Rotate() {};
	void start(const cv::Mat &image, const Bbox &finalBbox, cv::Mat& rotRes);
private:
	void getsrc_roi(std::vector<cv::Point2f> x0, std::vector<cv::Point2f> dst, cv::Mat& roi);
};