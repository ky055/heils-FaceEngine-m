#pragma once
#include "math.h"
#include <algorithm>

#define FEATURE_SIZE 512


extern float calculSimilar(const float *v1, const float *v2, int _featureSize);