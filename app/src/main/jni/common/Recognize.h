#pragma once
#include <string>
#include "net.h"
#include "math.h"
#include <algorithm>


class Recognize {
public:
	Recognize(std::string);
	~Recognize();
	void start(const ncnn::Mat&, float*);
	void SetThreadNum(int threadNum);
private:
	ncnn::Net Recognet;
	std::vector<float> feature_out;
	int threadnum = 1;
};


