#include "EngineUtils.h"
float calculSimilar(const float *v1, const float *v2,int _featureSize) {
	float ret = 0.0, mod1 = 0.0, mod2 = 0.0;
	for (int i = 0; i < _featureSize; ++i) {
		ret += v1[i] * v2[i];
		mod1 += v1[i] * v1[i];
		mod2 += v2[i] * v2[i];
	}
	return ret / sqrt(mod1) / sqrt(mod2);
}