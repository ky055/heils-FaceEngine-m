#pragma once
#include "opencv2/opencv.hpp"

struct Bbox {
	float score;
	int x1, y1, x2, y2;//left, top, right, bottom
	float area;
	float ppoint[10];
	float regreCoord[4];
};

bool cmpScore(Bbox lsh, Bbox rsh);

bool cmpArea(Bbox lsh, Bbox rsh);

float distance(cv::Point p1, cv::Point p2);

