#pragma once
#include <opencv2/opencv.hpp>
#include "net.h"
#include <string>
#include <vector>

class Mark
{
public:
	Mark(std::string);
	~Mark();
	void getMarkPoint(const cv::Mat&, std::vector<float>&);
private:
	ncnn::Net squeezenet;
	float mean_vals[3] = { 128.f, 158.f, 158.f };
};


