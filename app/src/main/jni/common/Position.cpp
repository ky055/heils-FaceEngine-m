#include "Position.h"
#include <cmath>
#define pi 3.14159265

Position::Position(Bbox finalBbox, std::vector<float> markPoint)
{
	if (!cPoint5.empty()) cPoint5.clear();
	cPoint5.push_back(cv::Point(finalBbox.ppoint[0], finalBbox.ppoint[5]));
	cPoint5.push_back(cv::Point(finalBbox.ppoint[1], finalBbox.ppoint[6]));
	cPoint5.push_back(cv::Point(finalBbox.ppoint[2], finalBbox.ppoint[7]));
	cPoint5.push_back(cv::Point(finalBbox.ppoint[3], finalBbox.ppoint[8]));
	cPoint5.push_back(cv::Point(finalBbox.ppoint[4], finalBbox.ppoint[9]));
	if (!cPoint9.empty()) cPoint9.clear();
	cPoint9.push_back(cv::Point((markPoint[72] + markPoint[78]) / 2, (markPoint[73] + markPoint[79]) / 2));
	cPoint9.push_back(cv::Point((markPoint[84] + markPoint[90]) / 2, (markPoint[85] + markPoint[91]) / 2));
	cPoint9.push_back(cv::Point(markPoint[0], markPoint[1]));
	cPoint9.push_back(cv::Point(markPoint[32], markPoint[33]));
	cPoint9.push_back(cv::Point(markPoint[16], markPoint[17]));
	cPoint9.push_back(cv::Point(markPoint[60], markPoint[61]));
	cPoint9.push_back(cv::Point(markPoint[96], markPoint[97]));
	cPoint9.push_back(cv::Point(markPoint[108], markPoint[109]));
	cPoint9.push_back(cv::Point((markPoint[38] + markPoint[48]) / 2, (markPoint[39] + markPoint[49]) / 2));
}


Position::~Position()
{
}

cv::Point getRotatePoint(cv::Mat srcImage, cv::Point Point, const cv::Point rotate_center, const double angle) {
	cv::Point dstPoint;
	int x1 = 0, y1 = 0;
	int row = srcImage.rows;
	x1 = Point.x;
	y1 = row - Point.y;
	int x2 = rotate_center.x;
	int y2 = row - rotate_center.y;
	int x = cvRound((x1 - x2)*cos(pi / 180.0 * angle) - (y1 - y2)*sin(pi / 180.0 * angle) + x2);
	int y = cvRound((x1 - x2)*sin(pi / 180.0 * angle) + (y1 - y2)*cos(pi / 180.0 * angle) + y2);
	y = row - y;
	dstPoint = cv::Point(x, y);
	return dstPoint;
}

int Position::getPos() {
	float chinDirection = abs(1.0 * (cPoint9[1].y - cPoint9[0].y) / (cPoint9[1].x - cPoint9[0].x + 1e-8));
	float nosePointing = distance(cPoint9[5], cPoint9[2]) / distance(cPoint9[5], cPoint9[3]);
	if (cPoint5[2].x > cPoint5[1].x || cPoint5[2].x < cPoint5[0].x) return 1;//Ťͷ
	if (nosePointing > 1.2 || nosePointing < 0.8) return 2;//����
	if (chinDirection > 0.3) return 3;//��ͷ
	return 0;
}


//left_eye 0, right_eye 1, left_ear 2, right_ear 3
//chin 4, nose 5, left_mouth 6, right_mouth 7, center_high 8

cv::Mat Position::cropByPosition(cv::Mat rawImage) {
	cv::Point midEye((cPoint9[0].x + cPoint9[1].x) / 2, (cPoint9[0].y + cPoint9[1].y) / 2);
	cv::Point eyeDirection(cPoint9[1].x - cPoint9[0].x, cPoint9[1].y - cPoint9[0].y);
	double rotation = -atan2(eyeDirection.y, eyeDirection.x);
	double angle = -rotation * 180 / 3.14;
	//rows-height  cols-width

	cv::Point center(rawImage.cols / 2, rawImage.rows / 2);
	cv::Rect newBound = cv::RotatedRect(center, rawImage.size(), angle).boundingRect();
	
	cv::Mat newImage;
	cv::Mat rot = cv::getRotationMatrix2D(center, angle, 1);
	rot.at<double>(0, 2) += newBound.width / 2.0 - center.x;
	rot.at<double>(1, 2) += newBound.height / 2.0 - center.y;
	cv::Point newMidEye = getRotatePoint(rawImage, midEye, center, angle);
	int newEyex = (newBound.width - rawImage.cols) / 2 + newMidEye.x;
	int newEyey = (newBound.height - rawImage.rows) / 2 + newMidEye.y;
	cv::Point finalEye(newEyex, newEyey);
	cv::warpAffine(rawImage, newImage, rot, newBound.size());

	/*cv::circle(rawImage, midEye, 5, cv::Scalar(0, 0, 255));
	cv::circle(newImage, finalEye, 5, cv::Scalar(0, 0, 255));
	cv::imshow("1", rawImage);
	cv::imshow("2", newImage);
	cv::waitKey(0);*/

	float earDistance = distance(cPoint9[2], cPoint9[3]);
	float chinDistance = distance(midEye, cPoint9[4]);
	float cropDistance = std::max(earDistance, chinDistance);

	float t = cos(rotation);
	cropDistance *= std::cos(rotation);
	int x0, x1, y0, y1;
	if (int(finalEye.x - cropDistance) < 0) x0 = 0;
	else x0 = finalEye.x - cropDistance;
	if (int(finalEye.y - cropDistance) < 0) y0 = 0;
	else y0 = finalEye.y - cropDistance;
	if (int(finalEye.x + cropDistance) > newImage.cols) x1 = newImage.cols;
	else x1 = finalEye.x + cropDistance;
	if (int(finalEye.y + cropDistance) > newImage.rows) y1 = newImage.rows;
	else y1 = finalEye.y + cropDistance;

	cv::Rect cropBound(x0, y0, x1 - x0, y1 - y0);
	cv::Mat cropRes = newImage(cropBound);

	return cropRes;
}