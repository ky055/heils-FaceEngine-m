#pragma once
#include <opencv2/opencv.hpp>
#include "Struct.h"
#include <vector>

class Position
{
public:
	Position(Bbox finalBbox, std::vector<float> markPoint);
	~Position();
	int getPos();
	cv::Mat cropByPosition(cv::Mat rawImage);
private:
	std::vector<cv::Point> cPoint9;
	std::vector<cv::Point> cPoint5;
	//std::vector<myPoint> mark2cPoint(std::vector<float> markPoint);
};
