#include <android/log.h>
#include <android/bitmap.h>
#include <jni.h>
#include <iostream>
#include <fstream>    // file stream
#include <dirent.h>
#include <sstream>
#include <vector>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <HeilsFaceEngine.h>
#include <common/EngineUtils.h>

#define TAG "HeilsSo"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,TAG,__VA_ARGS__)

using namespace std;

extern "C" {
HeilsFaceEngine *m_HeilsFaceEngine;

JNIEXPORT void JNICALL
Java_cn_heils_engine_Engine_initEngine(JNIEnv *env, jobject thiz, jstring modelPath) {

    std::string str = (std::string) env->GetStringUTFChars(modelPath, NULL);
    LOGD("start tensorflowRun Init %s\n", str.c_str());
    init_engine(&m_HeilsFaceEngine);
    env->DeleteLocalRef(modelPath);
}

JNIEXPORT jint JNICALL
Java_cn_heils_engine_Engine_cropFace(JNIEnv *env, jobject thiz, jstring faceImg1, jstring faceImg2) {

//    std::string face = (std::string) env->GetStringUTFChars(faceImg1, NULL);
//    std::string face_crop = (std::string) env->GetStringUTFChars(faceImg2, NULL);
//    int result = m_HeilsFaceEngine->FaceCropImage(face.c_str(), face_crop.c_str());
    return 0;
}

JNIEXPORT jint JNICALL
Java_cn_heils_engine_Engine_getFeature(JNIEnv *env, jobject thiz, jstring faceImg, jfloatArray faceFeature) {

    std::string face_path = (std::string) env->GetStringUTFChars(faceImg, NULL);
    float* feature = new float[FEATURE_SIZE];
    int result = m_HeilsFaceEngine->FaceFeature(face_path.c_str(), feature);
    env->SetFloatArrayRegion(faceFeature,0,FEATURE_SIZE,feature);
    delete feature;
    return result;
}



JNIEXPORT jfloat JNICALL
Java_cn_heils_engine_Engine_comparePersons(JNIEnv *env, jobject thiz, jstring faceImg1, jstring faceImg2) {

    std::string face_1 = (std::string) env->GetStringUTFChars(faceImg1, NULL);
    std::string face_2 = (std::string) env->GetStringUTFChars(faceImg2, NULL);
    float result = m_HeilsFaceEngine->CalSimilar(face_1.c_str(), face_2.c_str());
    return result;
}

JNIEXPORT void JNICALL
Java_cn_heils_engine_Engine_destroyEngine(JNIEnv *env, jobject thiz) {
    destroy_engine(&m_HeilsFaceEngine);
}
}