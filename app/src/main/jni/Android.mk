LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
OPENCVROOT:= F://local//Android_3rdparty//opencv-3.3.0-android-sdk//OpenCV-android-sdk//sdk//native
OPENCV_INSTALL_MODULES:=on
OPENCV_LIB_TYPE:=SHARED
include ${OPENCVROOT}/jni/OpenCV.mk

include $(CLEAR_VARS)
LOCAL_MODULE := libncnn
LOCAL_SRC_FILES := $(LOCAL_PATH)/ncnn/libs/$(TARGET_ARCH_ABI)/libncnn.a
include $(PREBUILT_STATIC_LIBRARY)



LOCAL_SRC_FILES := engine_jni.cpp \
                   HeilsFaceEngine.cpp \
                   common/Detect.cpp \
                   common/EngineUtils.cpp \
                   common/Face.cpp \
                   common/FaceEngine.cpp \
                   common/Mark.cpp \
                   common/MutexLock.cpp \
                   common/Position.cpp \
                   common/Posture.cpp \
                   common/Recognize.cpp \
                   common/Rotate.cpp \
                   common/Struct.cpp


LOCAL_LDLIBS += -llog
LOCAL_MODULE := HeilsFaceEngine


LOCAL_C_INCLUDES := $(LOCAL_PATH)/ncnn \
                    $(LOCAL_PATH)/include \
                    ${OPENCVROOT}/jni/include


LOCAL_STATIC_LIBRARIES := libncnn




LOCAL_CFLAGS := -O2 -fomit-frame-pointer -fstrict-aliasing -ffunction-sections -fdata-sections -ffast-math -mfloat-abi=softfp -mfpu=neon
LOCAL_CPPFLAGS := -O2  -fomit-frame-pointer -fstrict-aliasing -ffunction-sections -fdata-sections -ffast-math -fexceptions -mfloat-abi=softfp -mfpu=neon
LOCAL_LDFLAGS += -Wl,--gc-sections

LOCAL_CFLAGS += -fopenmp
LOCAL_CPPFLAGS += -fopenmp
LOCAL_LDFLAGS += -fopenmp
LOCAL_CPPFLAGS += -frtti
LOCAL_CPPFLAGS += -std=c++11 -D__cplusplus=201103L

LOCAL_LDLIBS := -lz -llog -ljnigraphics


include $(BUILD_SHARED_LIBRARY)