#pragma once
#include <string.h>
#include <iostream>
#include <iostream>
#include <android/log.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include <common/FaceEngine.h>


#define TAG "HeilsSo"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,TAG,__VA_ARGS__)


class HeilsFaceEngine
{
private:
    FaceEngine* m_faceEngine = NULL;

public:
    HeilsFaceEngine();
    virtual ~HeilsFaceEngine();

    void SetFaceDetectModel(const char* _modelDir);
    void SetFaceFeatureModel(const char* _modelDir);
    void SetFaceMarksModel(const char* _modelDir);
    void SetMinFaceSize(int _minSize);

    int FaceFeature(const char* _inputImgPath, float* _faceFea);
    int FaceCropImage(const char* _inputImgPath, const char* _outImgPath);
    float CalSimilar(const char* _inputImgPath1, const char* _inputImgPath2);


};

extern "C" {

void init_engine(HeilsFaceEngine** _heilsEngine);

void destroy_engine(HeilsFaceEngine** _heilsEngine);

}
