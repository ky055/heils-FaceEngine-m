package cn.heils.engine;

public class Engine {

    static {
        System.load("HeilsFaceEngine");
    }

    public native void initEngine(String modelPath);

    public native int cropFace(String facePath1, String facePath2);

    public native int getFeature(String faceImg, float[] faceFeature);

    public native float comparePersons(String facePath1, String facePath2);

    public native void destroyEngine();
}
